---
layout: handbook-page-toc
title: Product Manager Toolkit
---

## On this page
{:.no_toc}

- TOC
{:toc}

- - -

## What is this page for ?

As a Product Manager, you constantly have data-related questions. You want to know how your feature is performing, who are your top users are, how many self-managed instances have adopted a specific feature, all of these could be broken down by product tier, paid vs free, edition...

We have of course a lot of data, but some of you are not fully proficient or comfortable with SQL. And even though you can try to copy some of the existing charts to tune them to get the data you expect, you will waste some time, will never be 100% sure if your chart shows accurate data and will get frustrated.

The Data Team has created a set of different snippets to address your needs. This list is not comprehensive and is mainly based on the feedback, questions and tasks we received from Product Managers. All of these snippets will be explained with detailed examples on how to change them and adapt them to your own needs. This is also at the moment mainly focusing on Self-Managed data and usage ping data source.

If you prefer to learn by doing, we provide [a documented example SiSense dashboard](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations) that you can check out, most widgets include documentation in their SQL edit view too to help you customize them. You can look at the code used to generate the charts. The code has some extra guidelines commented out that will help you better understand how the given snippet works and what you can achieve with it.

Overall, these snippets are meant to help any team member who is looking for specific data from Usage Ping. We want this page and these snippets to be a success which means to be understood, used and enhanced. We created [an issue](https://gitlab.com/gitlab-data/analytics/-/issues/7738) to gather feedbacks. So if you have some time, ideas, we will be super happy to read your thoughts and comments around this page and these snippets.

If you want to:

- have your Estimated xMAU metric charted, you should use the [td_xmau] snippet
- have an Estimated value for one of your Performance Indicators, you can explore the [td_xmau] snippet
- get a monthly count of instances using one of your feature with the [td_xmau_metrics_instance_count] snippet
- get an adoption rate (per feature) of your instance, please read this section regarding the [td_xmau_metrics_instance_adoption]
- get a list of your Top Users, Top Growing Users, or Churning Users, you can use either the [td_xmau_metrics_top_users], the [td_xmau_metrics_increasing_usage_users] or the [td_xmau_metrics_decreasing_usage_users]
- have more insights into a drop or an increase of your feature usage. The [td_xmau_monthly_change_breakdown] is designed for this purpose

## Trusted Data snippets

NOTE:
All these snippets start with a `td_` prefix. `td_` stands for Trusted Data, see [here for more details](https://about.gitlab.com/handbook/business-technology/data-team/platform/#tdf).

### Estimated xMAU or PI chart: [td_xmau(user segment, xMAU type, filter, target)]

The snippet [td_xmau] allows you to do it very quickly. With this snippet you will be able to:

- [get a standard Estimated xMAU chart as the ones embedded in the handbook](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10690032&udv=0)
- [break down your estimated xMAU by main_edition](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10690035&udv=0)
- [break down by Self-Managed product tier](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10690026&udv=0)

#### Usage

[We created this Google Spreadsheet](https://docs.google.com/spreadsheets/d/1VzQg9G46spnXxsjOLoQQFAYldh8UdUN5xaaK9t_x-Z8/edit?usp=sharing) to help you fill the snippet more easily. You just have to select some dimensions and then paste the snippet rendered in B6 in a New Chart in Periscope.

Parameters (use the Google Spreadsheet instead!):

- *user segment*: 'Paid' or 'All' users 
- *xMAU*: one of the xMAU types or 'PI'
- *filter*: if there are multiple options after choosing xMAU, you can choose the concrete metric
- *target*: the target to draw for this metric. 
    - If xMAU is 'PI', then the target value is taken from performance indicator yaml files' if [specified as described below](#how-to-show-dynamic-targets-on-your-widgets)
    - If the target it between 0-1, then it's considerd to be the a percentage change from the last value. 
    - If the target is greater than 1, then the target is added as a scalar.

Example chart: [Release Management - Estimated Paid GMAU](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10690032&udv=0)

#### You can actually do it with any of your metrics

The snippet [td_xmau] could actually be used with any metrics. The linked chart below is an example that charts the Estimated number of people using our wiki page.

Example chart: [wiki action - Estimated PI](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10787341&udv=0)

To build the snippet for your metric, you can go to [this spreadsheet](https://docs.google.com/spreadsheets/d/1VzQg9G46spnXxsjOLoQQFAYldh8UdUN5xaaK9t_x-Z8/edit?usp=sharing) and select for the column `which xMAU` `PI`.

Then as a filter you need to enter the metrics_path of the metrics you want to get your data from. [This section](#how-to-get-your-metrics_path-for-your-counter-) gives you clear guidelines on how to find your metrics_path.

### Recorded Monthly Metric Value Change: [td_xmau_metrics_recorded_metric_value(metrics_path)]

This snippet shows you the change in your metric value of any of your metric implemented in Usage Ping. 

#### Usage

1. Obtain the the metrics_path of the counter you want to monitor

   In [this view](https://app.periscopedata.com/app/gitlab/view/list_of_available_usage_ping_metrics_paths_for_mart__queries/5e6f74b0014747348c2525ba6e7d03bb/edit), type the name of your metric between the 2 `%` as shown in the example and then click on `Run SQL`. Copy the metrics_path from the output. You are going to use this in the snippet.

2. Use the metrics_path in the snippet

   Create a new chart to use the snippet. Input the copied metrics_path into the snippet. 
   
#### Adding a target value

You can add a target value to the chart by adding a `target_name` and a `monthly_estimated_targets` key to the performance indicator yaml [as described below](#how-to-show-dynamic-targets-on-your-widgets).

#### Example: 

- Metric: `g_project_management_issue_moved_monthly`
- metrics_path: `redis_hll_counters.issues_edit.g_project_management_issue_moved_monthly`
- Snippet in use:  `[td_xmau_metrics_recorded_metric_value('redis_hll_counters.issues_edit.g_project_management_issue_moved_monthly')]`

Then you will be able to get a dataset with the following potential breakdowns:

- delivery
- main_edition
- ping_product_tier
- umau_bucket

Example chart: [td_xmau_metrics_recorded_metric_value: Metric Value split by delivery](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10999660&udv=0)

### Recorded Monthly Metric Value Change with Total: [td_xmau_metrics_recorded_metric_value_with_total(metrics_path)]

This snippet extends `td_xmau_metrics_recorded_metric_value` with a total line.

#### Usage

[See above](#usage-1).

### Original Monthly Metric Value: [td_xmau_metrics_original_metric_value(metrics_path)]

This snippet is a variation of `td_xmau_metrics_recorded_metric_value` that shows to original metric, instead of a change from the previous period.

#### Usage

[See above](#usage-1).

### Monthly Active Instance chart: [td_xmau_metrics_instance_count(breakdown, metrics_path)]

The snippet [td_xmau_metrics_instance_count] shows you how many instances have used your feature in a given month. To get it working you just need to reference the `metrics_path`. 

#### Usage

To help you build the snippet, we created [this spreadsheet](https://docs.google.com/spreadsheets/d/1VzQg9G46spnXxsjOLoQQFAYldh8UdUN5xaaK9t_x-Z8/edit#gid=1727173902). You will need to just pick a value for each of the dropdowns in column B and then copy the value showed in B6 and put it in a New Chart in SiSense.

Example chart: [Number of Instances using CI Pipelines by Edition](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10755606&udv=0)

### Monthly Metrics Instance Adoption: [td_xmau_metrics_instance_adoption(breakdown, metrics_path)]

The snippet [td_xmau_metrics_instance_adoption](https://app.periscopedata.com/app/gitlab/snippet/td_xmau_instance_adoption/55f1e7266d0b47b2878e7b04d436197a/edit) is a snippet that can be used to get an idea about the Adoption of your features. It provides you the percentage of instances reporting back a specific usage ping metric. 

#### Usage 

To help you build the snippet, we created [this spreadsheet](https://docs.google.com/spreadsheets/d/1VzQg9G46spnXxsjOLoQQFAYldh8UdUN5xaaK9t_x-Z8/edit#gid=2130868735). You will need to just pick a value for each of the dropdowns in column B and then copy the value showed in BX and put it in a New Chart in SiSense.

Then you will be able to get a dataset with the following potential breakdowns:

- delivery
- main_edition
- ping_product_tier
- umau_bucket

### Insightful tables

The following snippets render a list of Top 50 Self-Managed Instances that can be used to find the corresponding customer to learn more about their use case. All these snippets follow the same pattern, you can [find the `metrics_path` used in the snippets as described above](#usage-1). 

[An example could be found in this chart](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10768540&udv=0)

#### Top Users Table: [td_xmau_metrics_top_users(metrics_path)]

The snippet renders a list of the Top 50 Self-Managed Instances using your specific feature. 

#### Instances with biggest MOM usage increase: [td_xmau_metrics_increasing_usage_users(metrics_path)]

The snippet renders a list of Top 50 Self-Managed Instances who have seen their monthly usage grow the most in the last completed month.

#### Instances with biggest MOM usage decrease: [td_xmau_metrics_decreasing_usage_users(metrics_path)]

The snippet renders a list of Top 50 Self-Managed Instances who have seen their monthly usage decrease the most in the last completed month. 

### Monthly change breakdown: [td_xmau_monthly_change_breakdown(metrics_path)]

Would like to be able to explain the growth/decrease of a specific monthly metric value? Have we seen big instances churning? Or just a overall decrease of usage due to some seasonality?

#### Usage

You can [find the `metrics_path` used for this snippets as described above](#usage-1). 

#### Example

Using the [td_xmau_monthly_change_breakdown] you will be able to create a chart that looks like this:
The snippet allows you to quickly see the number of tracked instances with decreasing, increasing usage or newly added by month.

Example chart: [Monthly breakdown change - Issues](https://app.periscopedata.com/app/gitlab/793297/xMAU-Analysis-Workflow---Example-Queries-and-Visualisations?widget=10768920&udv=0)

## How to show dynamic targets on your widgets?

The following snippets support adding dynamic target values from performance indicator yaml files:

- `td_xmau` with a PI metric type
- `td_xmau_metrics_recorded_metric_value`
- `td_xmau_metrics_recorded_metric_value_with_total`
- `td_xmau_metrics_original_metric_value`

### Usage

Adding the `target_name`, `monthly_recorded_targets`, `monthly_estimated_targets` keys to a performance indicator yaml file, shows the provided target values for the provided metric name.

- `target_name`: The target name should equal to the `metrics_path` used in the snippet
- `monthly_estimated_targets`: is a list of `"key": value` pairs where `key` is the target date to reach the target `value`
- `monthly_recorded_targets`: is a list of `"key": value` pairs where `key` is the target date to reach the target `value`

### Example

Having the following code snippet in a performance indicator yaml:

```yaml
...
- name: Configure:Configure - SMAU, GMAU - MAU of GitLab Managed Terraform State
  target_name: counts.projects_with_terraform_states
  monthly_recorded_targets:
    "2021-04-20": 5000
    "2020-11-01": 3000
    "2020-07-01": 700
  monthly_estimated_targets:
    "2021-04-20": 6000
    "2020-11-01": 3000
    "2020-07-01": 700
...
```

Then using a snippet like `[td_xmau_metrics_original_metric_value('counts.projects_with_terraform_states')]` shows the recorded targets on top of the original values. [See example](https://app.periscopedata.com/app/gitlab/677628/Infrastructure-as-Code?widget=9080255&udv=1037942).

## Go further...

With this official td_ (td stands for Trusted Data, see here for more details), we tried to create some key snippets that will help you get some quick charts and key data about your metrics. Of course, this list of snippet is just a glimpse of what can be done with our current data models.

If you have unanswered questions despite using these snippets, [we would love to hear your feedback](https://gitlab.com/gitlab-data/analytics/-/issues/7738).
