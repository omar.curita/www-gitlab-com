---
layout: handbook-page-toc
title: "Contributor Success Team"
description: "Contributor Success Team"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

- - -

## Mission

The goal of the team is to increase the technical improvements and efficiency of our contribution process to sustain our ambition of [1000+ contributors with merged MRs per month to GitLab](/company/strategy/#2-build-on-our-open-core-strength).

## Strategy

We will be executing in 4 key areas in support of the company's broader [dual-flywheels](/company/strategy/#dual-flywheels) strategy to attain more contributions. The 4 key areas together are the building blocks of our contributor & contribution 10x acceleration strategy. 

```mermaid

flowchart LR
  subgraph moreContributions["More Contributions"]
    contributorIncrease["Contributor Increase"]
    contributionIncrease["Contribution Increase"]
    increaseValue("Increase Contribution Value")
    improveVelocity("Improve Contribution Velocity")
    scaleCommunity("Scale the Community")
    expandOutreach("Expand Outreach")
    scaleCommunity-->improveVelocity
    scaleCommunity-->increaseValue
    expandOutreach-->scaleCommunity
    increaseValue-->contributionIncrease
    improveVelocity-->contributorIncrease
  end
  style moreContributions fill:#FFF, stroke:#9370DB, stroke-dasharray: 5 5
  style contributionIncrease fill:#9370DB,stroke:#9370DB,stroke-width:10px
  style contributorIncrease fill:#9370DB,stroke:#9370DB,stroke-width:10px
  style improveVelocity color:#6b4fbb, stroke:#9370DB
  style increaseValue color:#6b4fbb, stroke:#9370DB
  style expandOutreach color:#6b4fbb, stroke:#9370DB
  style scaleCommunity color:#6b4fbb, stroke:#9370DB
  click improveVelocity "./#improve-contribution-velocity" _self
  click increaseValue "./#increase-contribution-value" _self
  click expandOutreach "./#expand-outreach" _self
  click scaleCommunity "./#scale-the-community" _self

 ```


### Improve Contribution Velocity

Provide an outstanding and efficient contributor experience, from onboarding materials to final merge. Improve Contributor Journey, making it easier and efficient to contribute. Gather feedback from Wider community contributors and product teams on contribution friction.

#### Reduce Open Community MR Age

* **Why:** Improve the speed of contribution to production by reducing [Open community MR Age (OCMA)](/handbook/engineering/quality/performance-indicators/#open-community-mr-age) & review time. We have identified product groups with the highest open contribution MR age. Analysis and improvements are needed to address product groups with the biggest opportunity. In addition to improving MR review and gathering feedback.  
* **Epic:** [https://gitlab.com/groups/gitlab-com/quality/-/epics/1](https://gitlab.com/groups/gitlab-com/quality/-/epics/1)
* **DRI:** Contributor Success team (Engineering)

#### Simplify & improve contribution guides 

* **Why:** Make contribution guides easy to navigate. Our current contribution guides are fragmented and can be hard for new contributors to navigate and understand. 
* **Epic:** TBD
* **DRI:** Community Relations team (Marketing)

#### Improve contribution tooling

* **Why:** Provide fast and efficient contributor experience via our tooling. Our contributor tooling needs to be optimized for contributor productivity
* **Epic:** [https://gitlab.com/groups/gitlab-com/quality/-/epics/2](https://gitlab.com/groups/gitlab-com/quality/-/epics/2)
* **DRI:** Engineering Productivity team (Engineering)

#### Product groups focus

* **Why:** Contribution submissions, backlog, and technology stack vary amongst all [product groups](/company/team/structure/#product-groups). Providing a healthy community backlog alignment & establishing a common best practice outreach for all product groups.
* **Epic:** [https://gitlab.com/groups/gitlab-com/quality/-/epics/3](https://gitlab.com/groups/gitlab-com/quality/-/epics/3)
* **DRI:** Product Ops, Community Relations & Contributor Success 

### Increase Contribution Value

Incentivise, attract and retain contributors by providing a compelling value and regular recognition of contributors for their work. Contributor career advancement materials and awards.

#### Define contributor value proposition 

* **Why:** We need a clear definition of what drives people to contribute to GitLab and to present a compelling value proposition for increasing code contribution.
* **Epic:** TBD
* **DRI:** Community Relations team (Marketing)

#### Developer badges & certification 

* **Why:** Implement a badging system and eventually work towards a certification program for GitLab developers 
* **Epic:** [https://gitlab.com/groups/gitlab-com/quality/-/epics/4](https://gitlab.com/groups/gitlab-com/quality/-/epics/4)
* **DRI:** Contributor Success team (Engineering)

#### Contributor recognition

* **Why:** Provide sustained and impactful recognition to recognize & retain our contributors. Increase frequency and targeted recognition to types and persona of contributors
* **Epic:** [https://gitlab.com/groups/gitlab-com/quality/-/epics/7](https://gitlab.com/groups/gitlab-com/quality/-/epics/7)
* **DRI:** Contributor Success team (Engineering) & Community Relations team (Marketing)

#### Contributor career advancement

* **Why:** In Open Source Projects, the contributors’ motivation in addition to solving a bug or adding a missing feature is to get experience and build their CV for career advancement. 
* **Epic:** [https://gitlab.com/groups/gitlab-com/quality/-/epics/5](https://gitlab.com/groups/gitlab-com/quality/-/epics/5)
* **DRI:** Contributor Success team (Engineering) 

### Expand Outreach

Increase awareness with content and events to drive large amounts of contributors. Our outreach efforts so far have been limited. A proactive and focused effort to bring awareness and drive members from external communities will be required. Engineering to work along side Community Relations in expanded outreach events.

#### Increase contribution backlog exposure

* **Why:**  Contributing as a new member to a massive project can be overwhelming, which can lead to analysis paralysis and potentially losing contributors. We should offer a lens into a discoverable, sizable set of issues we can direct newcomers to. Consider using established 3rd party platforms.
* **Epic:** TBD
* **DRI:** Community Relations team (Marketing)

#### Scale contributor events

* **Why:** Build a sense of belonging, provide the social environment for contributors to have their voice, meet with their peers, share knowledge and celebrate.
* **Epic:** TBD
* **DRI:** Community Relations team (Marketing) & Contributor Success team (Engineering)

#### Community office hours


* **Why:** We need to scale office hour calls that have traditionally been a unique opportunity for product groups to provide support, guidance to code contributors as well as gather feedback.
* **Epic:** TBD
* **DRI:** Community Relations team (Marketing) & Contributor Success team (Engineering)

#### Increase social presence

* **Why:** Increase our social media presence beyond the currently limited mediums (Twitter, gitter), which will allow us to tap into existing developer communities.  
* **Epic:** TBD
* **DRI:** Community Relations team (Marketing)

### Scale the Community

Leverage the full-time customer contributor model and create wider community teams for rapid growth.

#### Create community teams

* **Why:** Create teams of wider community members with multiple domain expertise. We would like to depart from having contributors work single-handedly and create a team that can do more together.
* **Epic:** [https://gitlab.com/groups/gitlab-com/quality/-/epics/8](https://gitlab.com/groups/gitlab-com/quality/-/epics/8)
* **DRI:** Contributor Success team (Engineering) 

#### Customers as contributors

* **Why:** Increased contribution by our customers eventually builds towards customers having teams inside their organization contributing to GitLab. Engineering representative that can ride along with the evangelist and program manager in interaction with customers.
* **Epic:** [https://gitlab.com/groups/gitlab-com/quality/-/epics/9](https://gitlab.com/groups/gitlab-com/quality/-/epics/9)
* **DRI:** Community Relations team (Marketing) & Contributor Success team (Engineering)

#### Contribution specialization 

* **Why:** To surface contribution opportunities tailored to tech professions, implement frontend, backend, UX, Test and etc specialization in contribution types and MR coaches. This also allows a more aligned interaction between contributor and MR coaches of the same specialization.
* **Epic:** [https://gitlab.com/groups/gitlab-com/quality/-/epics/10](https://gitlab.com/groups/gitlab-com/quality/-/epics/10)
* **DRI:** Contributor Success team (Engineering)

## Performance Indicator Goals

We aim to increase the focus on our community contributions. Below is a timeline on how we will measure and track this goal.

- **[Unique Wider Community Contributors per Month](/handbook/engineering/quality/performance-indicators/#unique-wider-community-contributors-per-month)**
    - Target: increase to be greater than 200 per month by FY23Q4
    - Activities:
        - Partnership with Community Relations and Technical Marketing team.
        - Hold community office hours
        - Hold hackathons
        - Allow running of QA tests from forks
        - Shorten the CI runtime for community contributions (in forks)
- **[Open Community MR Age](/handbook/engineering/quality/performance-indicators/#open-community-mr-age)**
    - Target: decrease to lower than 30 days by FY23Q4
    - Activities:
        - Shorten CI time
        - Improve Community Contribution automation
        - Enable running QA tests on forks
        - Increase number of coaches
        - Partner with Engineering Productivity to provide feedback to improve contribution tooling (currently GDK).        
- **[MRARR](/handbook/engineering/quality/performance-indicators/#mrarr)**
    - Target: increase to 400M MR$ by FY23Q4
    - Activities:
        - Reach out to top tier enterprise customers
        - Partner with TAMs to enlist and facilitate contribution from paid customers
        - Launch contribution materials targeting large enterprises
        - Partner with Community relations team (David P)
        - Maintain a list of known contributors with a mapping to their accounts and the accounts ARR contribution as input to this KPI        
- **[Community Coaches per Month](/handbook/engineering/quality/performance-indicators/#community-mr-coaches-per-month)**
    - Target: increase to be greater than 50 per month by FY23Q3
    - Activities:
      - Work with Development Department (Christopher L, VP of Development) for volunteers.
      - Work with UX Department (Christie L, VP of UX) Christie for volunteers.
      - Refresh MR coaches as “Community coaches” so non-code review work can be encouraged (design, etc)
      - Launch training materials for coaches
- **[Community Contribution MRs as Features per Month](/handbook/engineering/quality/performance-indicators/#community-contribution-mrs-added-as-features-per-month)**
    - Target: increase to 30% by FY23Q2
    - Activities:
        - Encourage features at Community relations hackathons.
        - Published list of feature issues with Marketing team.

### FY23 Direction

In FY23 we will be focused on growing the number of unique new monthly
contributors, reducing OCMA and increasing MRARR. This will increase development
velocity without requiring additional engineering resources. Parallel to this
we'll increase observability of community contributed value through improving
the business intelligence around it. This will allow us to create some
predictability through foreshadowing. These efforts are cross-functional and
require working together with Marketing and Product Development.

This accounts for 70 - 80% of the workload. The remaining 20 - 30% is ad-hoc
work. The ad-hoc work is eclectic and ranges from supporting customers on
contributions, supporting various open source initiatives and supporting the
Engineering Productivity team.

#### Unique New Monthly Contributors

1. Minimize reliance on human interaction
1. Reduce volatility through introducing automations that drive contributions
   forward automatically
1. Capitalize on untapped potential - MRs that are active but not merged at the
   end of every single MR
1. Invest into attracting more new contributors

#### Community Contributed Value Observability

1. Introduce measurement points in various places in the contribution pipeline
1. Collect objective and subjective feedback from contributors about the process
1. Create insight into actual community contribution pipeline size
1. Standardize contribution MRARR valuation
1. Categorize contribution and measure value per type

#### Community Metrics Foreshadowing

1. Predict community metric trends
1. Empower teams to react to negative trends before they actualize
1. Define ambitious targets for FY24

#### OCMA

1. Minimize reliance on human factors that contribute to a large standard deviation
1. Support Engineering Productivity in driving OCMA down

## Team Responsibilities

* Carry out Engineering open source outreach efforts.
* Improve GitLab's Contribution Efficiency and Merge Request Coaching process.
* Engineering representative that can ride along with the evangelist and program manager.
* Organize community contributors into [community teams](team-structure.html) and ensure their success.
* Provide guidance to community contributors on technical and non-technical aspects.
* Track [contribution](/community/contribute/) delivery of the Community Contributors and Cohorts.
* Improve community recognition system and awards in collaboration with Community Relations team.
* Nominate impactful community contributors and contributions for recognition.
* Contribute as an MR Coach in one or more MR Coach specialties.
* Provide guidance and coaching to team members on technical contributions, product architecture, and other areas.
* Be a point of escalation for community contributors and identify GitLab DRIs to resolve blockers
* Participate in GitLab's overall open source outreach events and processes.
* Collaborate closely with our Marketing counterparts and Core team.

## OKRs

Every quarter, the team commits to [Objectives and Key Results (OKRs)](/company/okrs/). The below shows current and previous quarter OKRs, it updates automatically as the quarter progresses.

### Current quarter

<iframe src="https://app.ally.io/public/TQRmKHpOC7NoZb7" class="dashboard-embed" height="1600" width="100%" style="border:none;"> </iframe>

### Previous quarter
<!-- uncomment after FY23Q2 starts
<iframe src="https://app.ally.io/public/e0izWt13OqmGHuO" class="dashboard-embed" height="1600" width="100%" style="border:none;"> </iframe>
--> 

## Project Management

This team has the following immediate work items planned.

* [Contribution Efficiency Improvements Epic](https://gitlab.com/groups/gitlab-com/-/epics/1619)
* [Increasing MRARR through internal partnerships](https://gitlab.com/groups/gitlab-com/-/epics/1225)

## Team Structure

This new team resides under the Quality Department operating as a team of
Full-stack engineers, led by an Engineering Manager reporting to the
Quality Department Leader.

```mermaid
graph TD
    A[Quality Department]
    A --> B[Contributor Success Team]
    B --> BA[Contributor Success Engineer]
    B --> BB[Contributor Success Engineer]
    A --> CA(Quality Engineering teams)
    CA --> CAA[Software Engineer in Test]
    CA --> CAB[Software Engineer in Test]
    CA --> CAC[Software Engineer in Test]
    A --> D[Engineering Productivity Team]
    D --> DA[Engineering Productivity Engineer]
    D --> DB[Engineering Productivity Engineer]
    style B fill:#bbf,stroke:#f66,stroke-width:2px, color:#fff
    style BA fill:#bbf,stroke:#f66,stroke-width:2px, color:#fff
    style BB fill:#bbf,stroke:#f66,stroke-width:2px, color:#fff
```

## Contributor Efficiency Working group

There's a working group with members from Quality and Community Relations that
aims to streamline and improve contributor efficiency. It implements key
business iterations that results in substantial and sustained increases to
community contributors & contributions.

- Handbook page - <https://about.gitlab.com/company/team/structure/working-groups/contribution-efficiency/>
- Agenda - <https://docs.google.com/document/d/1AOgqaslnq-WI1ICSZ1NzSnALf1Va4D5qAD191icAoSI/edit#>

## Code Contributor User Journey

The code contributor user journey is documented in the handbook - [User Journey](./user-journey.html)
