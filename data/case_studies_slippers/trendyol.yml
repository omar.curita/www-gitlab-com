title: Trendyol
file_name: trendyol
canonical_path: /customers/trendyol/
cover_image: /images/blogimages/trendyol_cover_image.jpg
cover_title: |
  GitLab enables major e-commerce player Trendyol to boost developer productivity and create market expansion
cover_description: |
  Speeding development and deployments with GitLab platform, Trendyol is able to expand business-critical online customer services
twitter_image: /images/blogimages/trendyol_cover_image.jpg
twitter_text: GitLab enables major e-commerce player to boost developer productivity and create market expansion @trendyol
customer_logo: /images/case_study_logos/trendyol_logo_correct.png
customer_logo_css_class: brand-logo-tall
customer_industry: E-Commerce
customer_location: Istanbul, Turkey
customer_solution: GitLab Self Managed Premium 
customer_employees: |
  1300 developers
customer_overview: |
 Trendyol developer teams rely on GitLab to turbocharge software deployment, driving market expansion
customer_challenge: |
  Fast-growing e-commerce retailer needed a consolidated DevOps platform that would scale and support ambitious business goals
key_benefits: >-

  
    Increased developer productivity with ready access and self-service   

  
    Tool chain simplification

  
    Automated documentation creation

  
    Easy integration with LDAP, Slack, K8s & Pod Logs

  
    Productive Professional Services partnership


    Easy AWS Integration and Deploymen
customer_stats:
  - stat: 30% 
    label: Improvement in developer productivity
  - stat:  60%
    label: Reduction in build times (from 10 mins to 4 mins)
  - stat:  50%
    label: Faster to launch a new application via a single platform (from 2 days to 1 day)

customer_study_content:
  - title: the customer
    subtitle: Trendyol at forefront of Turkish e-commerce expansion
    content: >-
  
        Headquartered in Istanbul, [Trendyol](https://www.trendyol.com) is the largest e-commerce company in Turkey; operating an R&D center, a last-mile delivery system, mobile wallets, and a wide-ranging marketplace that offers everything from food to clothing, electronics, and cosmetics.  As part of its mission, the company exports Turkish products over various platforms to consumers around the world. The company is focused on making its mark by dependably serving more than 30 million shoppers and delivering more than 1 million packages every day. To get all of this done, the company uses natural language processing, machine learning, recommendation systems, and big data. And state-of-the-art DevOps processes are vital to achieving Trendyol’s business strategy.


  - title: the challenge
    subtitle:  Adapting to change, tackling inefficiencies
    content: >-
    

        As Trendyol has expanded its stable of services and platforms, its developer teams amassed a diverse and complex assortment of DevOps tools. This caused obvious inefficiencies. Learning and using multiple tools was time-consuming and ineffective for the development teams, even slowing down developer onboarding just as the company needed to expand its developer ranks. It also ultimately slowed down deployments. The company’s DevOps managers needed to change that 

        It was clear that using multiple tools, like BitBucket, Jenkins, and GitLab, couldn’t continue. They needed one platform.  Because of compliance issues, it needed to be a CI/CD engine. To make all of this happen, Trendyol selected GitLab Premium for a  container-based, runner architecture, enabling faster and continuous deployment. The platform’s simplicity was the primary deciding factor.  “We knew we wanted to consolidate tools into a single tool so our development team could spend less time context-switching and learning new platforms, and more time improving our product.”  added Cenk Çivici, CTO, Trendyol.


  - title: the solution
    subtitle:  GitLab enables disaster recovery, high availability
    content: >-


        Implementing GitLab Premium with OpenStack and the AWS Cloud Platform has provided Trendyol teams with high availability and disaster recovery. GitLab Premium has allowed those DevOps teams to simplify operations and organize using a single resource. And advanced search capabilities enabled critical collaboration. Now, as new developers start work at Trendyol, they are not faced with learning multiple tools and repeatedly shifting into different environments, according to Cenk Civici, Trendyol’s CTO who called GitLab “a very mature on-premises solution.”  “GitLab included all the processes needed to execute a project within a single platform,” he added.

      

  - title: the results
    subtitle:  Faster development enables critical business strategy
    content: >-

        Adopting the GitLab platform has meant a 30% improvement in developer productivity, which has translated into a dramatic market expansion. The developer experience is significantly enhanced with this standardization, providing a combined repository, registry, and CI/CD view for the developer, and reliable operations updates for DevOps managers.  New developer onboarding times have reduced from 10-8 days as a result of using one DevOps platform and not a disparate toolchain. This high level of integration means GitLab can operate together with LDAP, JIRA, Slack, and similar applications. And streamlined integration with Kubernetes helps Trendyol teams accelerate the addition of new platform features and updates. “GitLab included all the processes needed to execute a project within a single platform” explained Cenk Civici, CTO, Trendyol


        GitLab automation means developers are no longer burdened with many manual documentation tasks, while GitLab advanced search allows them to more easily reuse and share code, and learn from each other in forums. For crucial pipeline configurations, GitLab’s simple YAML-based processes ensure repeatable processes, while its container-based runner architecture easily handles 70 or more Kubernetes clusters. GitLab features, such as Service Desk and Issues, provide additional productivity. GitLab’s priority support further assures 24/7 support for the vital infrastructure that powers production services. Trendyol’s DevOps and platform teams now are successfully deploying infrastructure as code, and expanding resources into multiple data centers.  Headquartered in Istanbul, Trendyol is the largest e-commerce company in Turkey; operating an R&D center, a last-mile delivery network, instant food and grocery delivery service, and a wide-ranging marketplace that offers everything from food to clothing, electronics, and cosmetics. Launching a new application has become 50% quicker via GitLab; now taking just 1 day when it used to take 2 days.


        GitLab has become a key part of how the company creates a robust platform that drives its business strategy to become one of the largest e-commerce companies in the world, offering a full range of customer services.


   

        ## Learn more about GitLab solutions
    
  
        [Security with GitLab](/solutions/dev-sec-ops/)
    
  
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: GitLab’s features allowed us to speed up our production cycle, which accelerated our development and deployment process
    attribution: Cenk Civici
    attribution_title: Chief Technology Officer, Trendyol












